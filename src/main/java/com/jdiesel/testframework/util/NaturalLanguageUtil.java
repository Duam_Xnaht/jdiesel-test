package com.jdiesel.testframework.util;

public class NaturalLanguageUtil {
	public static String naturalLanguageToEnumCase(String naturalLanguage) {
		return naturalLanguage.replace(' ', '_').toUpperCase();
	}
}
