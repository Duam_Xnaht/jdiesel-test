package com.jdiesel.testframework.documentation;

public class DieselDocumentationGenerationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public DieselDocumentationGenerationException(String exception) {
		super(exception);
	}
}
