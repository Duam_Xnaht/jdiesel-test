package com.jdiesel.testframework.documentation.dto;

import java.util.List;

public class JsonReportResultsDto {

	private String title;
	private List<StepReport> afterStorySteps;
	private List<StepReport> beforeStorySteps;
	private String path;
	private List<ScenarioReport> scenarios;
	
	public String getTitle() {
		return title;
	}
	public List<StepReport> getAfterStorySteps() {
		return afterStorySteps;
	}
	public List<StepReport> getBeforeStorySteps() {
		return beforeStorySteps;
	}
	public String getPath() {
		return path;
	}
	public List<ScenarioReport> getScenarios() {
		return scenarios;
	}
}
