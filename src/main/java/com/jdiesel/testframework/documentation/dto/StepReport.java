package com.jdiesel.testframework.documentation.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown=true)
public class StepReport {

	private String outcome;
	private String value;
	private String failure;
	private String keyword;
	
	
	public String getOutcome() {
		return outcome;
	}
	public String getValue() {
		return value;
	}
	public String getFailure() {
		return failure;
	}
	public String getKeyword() {
		return keyword;
	}

}
