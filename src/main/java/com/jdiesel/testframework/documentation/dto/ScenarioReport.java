package com.jdiesel.testframework.documentation.dto;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
@JsonIgnoreProperties(ignoreUnknown = true)
public class ScenarioReport {
	private String title;
	private String failure; //Stack trace
	private String keyword; //Scenario, Scenario Outline, etc
	private List<Map<String, String>> steps;
	
	public String getTitle() {
		return title;
	}
	public String getFailure() {
		return failure;
	}
	public String getKeyword() {
		return keyword;
	}
	public List<Map<String, String>> getSteps() {
		return steps;
	}
	
}
