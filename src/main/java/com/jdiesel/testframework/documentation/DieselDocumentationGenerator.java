package com.jdiesel.testframework.documentation;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.io.FileUtils;
import org.asciidoctor.Asciidoctor;
import org.asciidoctor.Attributes;
import org.asciidoctor.AttributesBuilder;
import org.asciidoctor.OptionsBuilder;
import org.asciidoctor.Placement;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jdiesel.testframework.documentation.dto.JsonReportResultsDto;
import com.jdiesel.testframework.documentation.dto.ScenarioReport;
import com.jdiesel.testframework.documentation.dto.StepReport;
import com.jdiesel.testframework.documentation.enums.Outcome;
import com.jdiesel.testframework.util.NaturalLanguageUtil;

import gherkin.lexer.Encoding;

/**
 * Provided the Formats.JSON option was used as a format with the
 * StoryReporterBuilder in the Runner configuration, that output can be consumed to
 * generate beautiful, powerful AsciiDoctor documents.
 * 
 * If using JUnit or some other test runner, it is best to use this class in the
 * "After All" phase of test execution (e.g @AfterClass, @AfterAll, etc)
 * @author nboyer
 *
 */
public class DieselDocumentationGenerator {

	private final String jsonDirectory = "./target/jbehave";
	private final String outputDirectory = "./target/jbehave/";
	private final String BLOCK = "==========";
	private final String documentBeginningFileName = "documentation-forward.adoc";
	private final ObjectMapper mapper;
	private Path outputSourcePath;
	public DieselDocumentationGenerator() {
		mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
		mapper.setSerializationInclusion(Include.NON_NULL);
		outputSourcePath = Paths.get(outputDirectory + "DieselAsciiDoctorSource.adoc");
	}

	public void generateDocumentation(String adocId) {
		
		//Path adocSourcePath = Paths.get(outputDirectory + adocId + ".adoc");
		//File adocSource = new File(adocSourcePath.toString());
		try {
			
			// Find existing header adoc
			Optional<Path> documentForward = Files.walk(Paths.get("./"))
					.filter(file -> file.getFileName().toString().equals(documentBeginningFileName)).findAny();
			if (documentForward.isPresent()) {
				// Copy forward to beginning of the file which will
				// also contain all documented features
				//Files.copy(documentForward.get(), adocSourcePath, 
				new File(outputSourcePath.toString());
				Files.copy(documentForward.get(), outputSourcePath, 
						StandardCopyOption.REPLACE_EXISTING );

			} else {	
				appendToAdocSource("= Untitled\n\n");
				appendToAdocSource(String.format("Create a file named %s " 
						+ " somewhere in the project and provide a custom header", documentBeginningFileName));
				appendToAdocSource("Sample Content:\n" +
						"--\n" +
						"= Your Application Name\n\n" +
						"A description of your application\n\n" +
						"== Local Development\n\n" +
						"How to start the application for local development.\n" +
						"(CLI arguments, local setup for security, etc.\n\n" +
						"== Local Testing\n" +
						"How to Test the application" +
						"== Deploying to Test Environment\n" +
						"How to deploy snapshot to test environment\n" +
						"Generating authorization tokens, etc.\n" +
						"--");
			}
			appendToAdocSource("\n\n== Features");
		
			// Find a default document header if it exists
			Files.list(Paths.get(jsonDirectory)).filter((p) -> {
				if (Files.isRegularFile(p)) {
					return p.toString().toLowerCase().endsWith(".json");
				} else {
					return false;
				}
			}).forEach(p -> addFeatureAsAsciiDoctor(p));

			// Convert the adoc file we just made into an AsciiDoctor file
			String outputFilename = outputDirectory + adocId + ".html";
			File adocResult = new File(outputFilename);
			
			try (FileWriter renderedWriter = new FileWriter(adocResult)) {
				String asciidoctor = Asciidoctor.Factory.create()
						.convertFile(new File(outputSourcePath.toFile().toString()),
								OptionsBuilder.options().destinationDir(new File(outputDirectory)).toFile(true)
									.attributes(AttributesBuilder.attributes()
										.tableOfContents(true)
										.tableOfContents(Placement.RIGHT)
										.copyCss(true)
										.icons(Attributes.FONT_ICONS)
										.sectionNumbers(true)
										.docType("book")
										.icons("font")
										.sectionNumbers(true)
										.linkCss(false)
										.sectionNumbers(true)
										.hardbreaks(true)));
				//renderedWriter.write(asciidoctor);
			}
			new File(outputDirectory +"DieselAsciiDoctorSource.html").renameTo(new File(outputFilename));
		} catch (IOException e) {

			throw new DieselDocumentationGenerationException(
					"Error generating documentation from JSON file\n" + e.getMessage());
		}

		// Run ASCII Doctor on the file to generate the documentation
	}

	private void addFeatureAsAsciiDoctor(Path featurePath) {
		if (featurePath.getFileName().toString().equals("BeforeStories.json")
				|| featurePath.getFileName().toString().equals("AfterStories.json")) {
			return;
		}
		try {
			String body = FileUtils.readFileToString(new File(featurePath.toString()), Encoding.DEFAULT_ENCODING)
					.replaceAll(",\"pendingMethods[^\\]]*]", "");
			JsonReportResultsDto featureResult = mapper.readValue(body, JsonReportResultsDto.class);
			if (featureResult.getScenarios() != null) { // Skip BeforeStories.json, AfterStories.json, etc
				appendToAdocSource("\n=== " + featureResult.getTitle());
				// Before all
				if (!featureResult.getBeforeStorySteps().isEmpty()) {
					appendToAdocSource("\n==== Pre-Conditions");
					for (StepReport step : featureResult.getBeforeStorySteps()) {
						appendStep(step);
					}
				}
				// After all
				if (!featureResult.getAfterStorySteps().isEmpty()) {
					appendToAdocSource("\n==== Post-Conditions");
					appendToAdocSource("\n\n" + BLOCK);
					for (StepReport step : featureResult.getAfterStorySteps()) {
						appendStep(step);
					}
					appendToAdocSource("\n\n" + BLOCK);
				}
				// Scenarios
				for (ScenarioReport scenario : featureResult.getScenarios()) {
					appendToAdocSource("\n\n==== " + scenario.getTitle() + "\n");
					appendToAdocSource("\n\n" + BLOCK);
					//appendToAdocSource("\n\n" + scenario.getTitle());
					if (scenario.getSteps() == null && !scenario.getFailure().isEmpty()) {
						appendToAdocSource("\nIMPORTANT: " + scenario.getFailure() + "\n\n" + BLOCK);
						continue;
					}
					for (Map<String, String> step : scenario.getSteps()) {
						// There is a bug in JBehave that causes pendingSteps to be
						// put into the steps object. Until it is fixed
						// we need to manually check each object
						if (step.containsKey("value")) {
							appendStep(step);
						}
					}
					appendToAdocSource("\n" + BLOCK);
				}
			}

		} catch (IOException e) {
			throw new DieselDocumentationGenerationException(String.format("Error generating documentation from JSON file\n\t%s\n\t%s",
					featurePath, e.getMessage()));
		}
	}

	private void appendStep(Map<String, String> step) throws IOException {
		appendToAdocSource(step.get("keyword") != null ? "\n*" + step.get("keyword") + "*\n" : "\n");
		//Append step text
		String stepTextWithIcon = step.get("value");
;

		// builder.write("\n*" + step.getKeyword() + "* ::");
		// builder.write("\n" + step.getValue());
		Outcome outcome = Outcome.valueOf(NaturalLanguageUtil.naturalLanguageToEnumCase(step.get("outcome")));
		//Append appropriate Icon
		switch (outcome) {
		case FAILED:
			// builder.write(" icon:thumbs-down[role=\"green\"],title=\"Passed\"");
			stepTextWithIcon += " icon:times[role=\"red\",title=\"Failed\"]";
			stepTextWithIcon += ("\n" + BLOCK + "\n\nIMPORTANT: " + step.get("failure") + "\n" + BLOCK);
			break;
		case NOT_PERFORMED:
			stepTextWithIcon +=(" icon:ban[role=\"beige\",title=\"Not Performed\"]");
			break;
		case SUCCESSFUL:
			stepTextWithIcon +=(" icon:check[role=\"green\",title=\"Passed\"]");
			break;
		case PENDING:
			stepTextWithIcon +=(" icon:search-minus[role=\"orange\"title=\"Pending\"]");
			break;
		default:
			throw new IllegalArgumentException("Do not know how to handle step status of " + outcome.name());
		}
		appendToAdocSource(stepTextWithIcon);
	}
	
	private void appendStep(StepReport step) throws IOException {

		appendToAdocSource("\n*" + step.getKeyword() + "* ::");
		//Get step text
		String stepWithTextAndIcon = step.getValue();
		//Get icon
		Outcome outcome = Outcome.valueOf(NaturalLanguageUtil.naturalLanguageToEnumCase(step.getOutcome()));
		switch (outcome) {
		case FAILED:
			// builder.write(" icon:thumbs-down[role=\"green\"],title=\"Passed\"");
			stepWithTextAndIcon += (" icon:times[role=\"red\"],title=\"Failed\"");
			stepWithTextAndIcon +=("\n\nCRITICAL: " + step.getFailure() + "\n");
			break;
		case NOT_PERFORMED:
			stepWithTextAndIcon += " icon:ban[role=\"beige\",title=\"Not Performed\"";
			break;
		case SUCCESSFUL:
			stepWithTextAndIcon += " icon:check[role=\"green\"],title=\"Passed\"";
			break;
		case PENDING:
			stepWithTextAndIcon += (" icon:search-minus[role=\"orange\"title=\"Pending\"");
		default:
			throw new IllegalArgumentException("Do not know how to handle step status of " + outcome.name());
		}
		appendToAdocSource(stepWithTextAndIcon);
	}
	
	
	private void appendToAdocSource(String text) {
		try {
			Files.write(outputSourcePath, text.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e) {
		  throw new DieselDocumentationGenerationException("An error occured while making the AsciiDoctor file for the Diesel Documentation:\n" + e.getMessage());
		}
		
	}
}
