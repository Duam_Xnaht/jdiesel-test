package com.jdiesel.testframework.documentation.enums;

public enum Outcome {
	SUCCESSFUL,
	NOT_PERFORMED,
	FAILED,
	PENDING,
	IGNORABLE;
}
