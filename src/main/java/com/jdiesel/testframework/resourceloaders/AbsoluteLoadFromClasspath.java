package com.jdiesel.testframework.resourceloaders;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

import org.jbehave.core.io.IOUtils;
import org.jbehave.core.io.InvalidStoryResource;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.io.StoryResourceNotFound;

public class AbsoluteLoadFromClasspath extends LoadFromClasspath {
	
	private Charset charset = StandardCharsets.UTF_8;
	
	//Charset not visible from parent class
	public void setCharset(Charset charset) {
		this.charset = charset;
	}
	
	@Override
	protected InputStream resourceAsStream(String resourcePath) {
        InputStream stream;
        String prefix = "file:";
		try {
			if (resourcePath.startsWith(prefix, 0)) {
				resourcePath = resourcePath.substring(prefix.length());
			}
			stream = new FileInputStream(resourcePath);
		} catch (FileNotFoundException e) {
			 throw new StoryResourceNotFound(resourcePath, classLoader);
		}
        return stream;
    }
	
	@Override
    public String loadResourceAsText(String resourcePath) {
        InputStream stream = resourceAsStream(resourcePath);
        try {
             return IOUtils.toString(stream, charset, true);
        } catch (IOException e) {
            throw new InvalidStoryResource(resourcePath, stream, e);
        }
    }
}
