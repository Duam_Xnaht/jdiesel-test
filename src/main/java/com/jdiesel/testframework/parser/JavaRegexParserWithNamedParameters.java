package com.jdiesel.testframework.parser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jbehave.core.parsers.RegexStepMatcher;
import org.jbehave.core.parsers.StepMatcher;
import org.jbehave.core.parsers.StepPatternParser;
import org.jbehave.core.steps.StepType;

public class JavaRegexParserWithNamedParameters implements StepPatternParser {

	protected String getBeginningToken() {
		return "<";
	}
	
	protected String getEndToken() {
		return ">";
	}
	
	@Override
	public StepMatcher parseStep(StepType stepType, String stepPattern) {
		//List<Parameter> parameters = findParameters(escapingPunctuation);
		Pattern regexPattern = Pattern.compile(stepPattern);
		//JBehave requires that parameters have names. Since java regular expressions do not provide this feature,
		//We also cannot know ahead of time the maximum number of matching groups (no expression to match against)
		//instead we're providing names for an absurdly large number of parameters.
		
		//Extract names for named parameters
		List<String> namedParameters = new ArrayList<>();
		String startToken = getBeginningToken();
		String endToken = getEndToken();
		Pattern namedParameterMatcher = Pattern.compile(startToken + "[^" + endToken + "]*" + endToken);
		Matcher namedMatches = namedParameterMatcher.matcher(stepPattern);
		while(namedMatches.find()) {
			namedParameters.add(namedMatches.group());
		}
		//Add default names so that matching regex groups can be injected
		final int MAX_PARAMETERS = 64;
		List<String> namesSoJBehaveWillInjectParameters = new ArrayList<>(MAX_PARAMETERS);
		//First add any found named parameters
		
		for (Integer i=0; i < MAX_PARAMETERS; i++) {
			namesSoJBehaveWillInjectParameters.add(i.toString());
		}
		return new RegexStepMatcher(stepType, stepPattern, regexPattern,
			namesSoJBehaveWillInjectParameters.toArray(new String[0]));
	}
}
