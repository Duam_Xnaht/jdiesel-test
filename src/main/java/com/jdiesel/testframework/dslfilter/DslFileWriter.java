package com.jdiesel.testframework.dslfilter;

import java.util.List;

import org.jbehave.core.model.Meta;
import org.jbehave.core.model.Scenario;
import org.jbehave.core.model.Story;

public class DslFileWriter {

	 static String storyObjectToGherkin(Story story, List<String> beforeSteps) {
		StringBuilder fileContent = new StringBuilder();
		// Make the Feature tags
		Meta featureTags = story.getMeta();
		for (String property : featureTags.getPropertyNames()) {
			fileContent.append("@" + property + "\n");
		}

		// Create the Feature description
		fileContent.append("Feature: " + story.getDescription().asString() + "\n\n");
		//Provide a comment explaining the nature of the generated file.
		fileContent.append("\n#WARNING! DO NOT MODIFY!!!\n" +
				"#This is an automatically generated file used by the test framework.\n"
				+"#It will be deleted and replaced on the next test run.\n"
				+ "#All edits should be done to the original test files,\n"
				+ "#not anything in the \n"
				+ "#" + DslFilter.generatedResourcePath + "\n"
				+ "#directory!\n"
				//Make warning obvious by displaying nothing else on first page
				+ "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
		// Add before all steps
		// Current defect in JBehave prevents 'Background' for running for more 
		// than one scenario. Disabling until fixed.
		// if (story.getLifecycle().getBeforeSteps()fileContent.append("Background: " +
		// story.getLifecycle().getBeforeSteps(Scope.STEP) + "\n\n");
		// Add each scenario
		for (Scenario scenario : story.getScenarios()) {
			// Add before all (background) steps
			//List<String> beforeSteps = story.getLifecycle().getBeforeSteps(Scope.SCENARIO);
			if (beforeSteps.size() > 0) {
				if (beforeSteps.stream().anyMatch(s -> s.charAt(0) != '#')) {
					fileContent.append("Background:\n");
				} else {
					fileContent.append("#Background:\n");
				}
				for (String backgroundStep : beforeSteps) {
					fileContent.append("\t" + backgroundStep + "\n");
				}
				fileContent.append("\n");
			}

			// Add Scenario Meta
			for (String property : scenario.getMeta().getPropertyNames()) {
				fileContent.append("@" + property + "\n");
			}
			// Add scenario description
			fileContent.append((scenario.getExamplesTable().asString().equals("") ? "Scenario: " : "Scenario Outline: ")
					+ scenario.getTitle() + "\n");
			// Add each step
			for (String step : scenario.getSteps()) {
				fileContent.append("\t" + step + "\n");
			}
			if (!scenario.getExamplesTable().asString().equals("")) {
				fileContent.append("\nExamples:\n");
			}
			// If there is no examples table the below statement appends an empty string
			fileContent.append(scenario.getExamplesTable().asString() + "\n\n");

		}
		return fileContent.toString();
	}
	
	 static String storyObjectToJbehaveStory(Story story, List<String> beforeSteps, List<String> afterSteps) {
		return "";
	}
}
