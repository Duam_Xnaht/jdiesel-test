package com.jdiesel.testframework.dslfilter;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.jbehave.core.annotations.Alias;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.parsers.StepPatternParser;
import org.jbehave.core.steps.StepType;
import org.reflections.Reflections;

/**
 * The feature files are at the heart of the scalable testing framework. They
 * represent a <i>Domain Specific Language</i> that can scale both within the
 * application and across the application.
 * 
 * However, because the DSL is effectively an interface for remote and local
 * tests to implement, it is inevitable that some scenarios will have steps that
 * would be be superfluous to the tests that implement them.
 * 
 * Being forced to implement methods in an interface that you don't need
 * violates the Interface Segregation Principle. To solve this issue, the
 * feature files are processes as templates which intelligently mark the steps a
 * test actually uses. If it is not needed, it is simply commented out so that
 * it can still provide context, readability and understandability of the tests.
 * 
 * @author nboyer
 *
 */
public class StepDefinitionMatcher {

	private Map<Class<?>, List<String>> class2Matchers = new HashMap<>();

	public enum StepMatchResult {
		DIRECT_MATCH, // The step definition class contains a match
		INDIRECT_MATCH, // The step definition class does NOT contain a match, however another does
		NO_MATCH; // No definition matches
	}

	public StepDefinitionMatcher() {
		// Specify the step classes that will templates associated with them
		// Using reflection, find all classes annotated with the Domain Specific
		// Language step definition

		String dslDefinitionPath = System.getProperty("diesel.test.dsldefinitions.path");
		Reflections reflections = dslDefinitionPath == null ? new Reflections("") : new Reflections(dslDefinitionPath);
		Set<Class<?>> stepDefinitions = reflections.getTypesAnnotatedWith(DslStepDefinition.class);
		// Grab all the regex from the step matchers. Keep track of the step matchers
		// for a particular class

		for (Class<?> definition : stepDefinitions) {
			// Get the regex for each of the methods that act as step matchers

			// Get all the methods
			Method[] methods = definition.getMethods();
			List<String> classMatchers = new ArrayList<>(16);
			for (int i = 0; i < methods.length; i++) {
				// If the method has one of the step matcher annotations,
				// then grab its regex
				Method m = methods[i];
				if (m.isAnnotationPresent(Given.class)) {
					classMatchers.add(m.getAnnotation(Given.class).value());
				}
				if (m.isAnnotationPresent(When.class)) {
					classMatchers.add(m.getAnnotation(When.class).value());
				}
				if (m.isAnnotationPresent(Then.class)) {
					classMatchers.add(m.getAnnotation(Then.class).value());
				}
				if (m.isAnnotationPresent(Alias.class)) {
					classMatchers.add(m.getAnnotation(Alias.class).value());
				}
			}
			
			class2Matchers.put(definition, classMatchers);
		}
	}

	/**
	 * To make tests scale across numerous platforms (some of which may have
	 * functionality unique to itself) we need to be able to determine if the step
	 * is relevent to the current context. This is accomplished
	 * 
	 * @param stepDefinition
	 * @param step
	 * @return
	 */
	public StepMatchResult matchStepInDefinitions(List<Class<?>> stepDefinitions, String step, StepPatternParser parser) {
		// First see if specified stepDefinition class has a match
		// We do this first to avoid possible match collisions with other step matcher
		// classes
		List<String> specifiedClassSteps = new ArrayList<>(16);
		Set<Entry<Class<?>, List<String>>> indirectDefinitions = new HashSet<>(class2Matchers.entrySet());
		for (Iterator<Class<?>> i = stepDefinitions.iterator(); i.hasNext();) {
			Class<?> directDefinition = i.next();
			//Add the step definition to the list of steps specific to this context
			if (class2Matchers.containsKey(directDefinition)) {
				specifiedClassSteps.addAll(class2Matchers.get(directDefinition));
				//Remove it from the list of all step definitions so we will have a 
				//list with all the steps from *other* test contexts
				indirectDefinitions.remove(indirectDefinitions.stream()
						.filter(e -> e.getKey().equals(directDefinition))
						.findFirst().get());
			}	
		}
		//StepPatternParser parser =  new JavaRegexParserWithNamedParameters();
		// See if the specified classes contain any matches
		for (String classStep : specifiedClassSteps) {
			// Remove prefix from step we're checking (Given, when, etc)
			String stepPattern = step.substring(step.indexOf(' ') + 1);
		   if (parser.parseStep(StepType.IGNORABLE,
			    classStep).matches(stepPattern)) {
				return StepMatchResult.DIRECT_MATCH;
			}
		}
		// If not, see if any other step definition in the application matches
		for (Entry<Class<?>, List<String>> entry : indirectDefinitions) {
			for (String classStep : entry.getValue()) {
				String stepPattern = step.substring(step.indexOf(' ') + 1);
				if (parser.parseStep(StepType.IGNORABLE,
					    classStep).matches(stepPattern)) {
						return StepMatchResult.INDIRECT_MATCH;
				}
			}
		}
		// Otherwise, no matches were found
		return StepMatchResult.NO_MATCH;
	}
}