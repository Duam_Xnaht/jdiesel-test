package com.jdiesel.testframework.dslfilter;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
/**
 * Used to mark a class as having a step definition. More specifically, marks
 * a class as having "glue" code with @Given, @When, @Then, etc methods with
 * regular expressions. 
 * 
 * The test framework treats the step matchers and feature/story files as 
 * a domain specific language. In order to share these files with other test
 * runners, superfluous steps need to be commented out in order to follow
 * the interface segregation principle.
 * @author nboyer
 *
 */
public @interface DslStepDefinition {

}
