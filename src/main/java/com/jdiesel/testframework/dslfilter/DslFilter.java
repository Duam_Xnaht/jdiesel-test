package com.jdiesel.testframework.dslfilter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.jbehave.core.annotations.Scope;
import org.jbehave.core.annotations.UsingSteps;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.embedder.PerformableTree;
import org.jbehave.core.embedder.StoryManager;
import org.jbehave.core.embedder.StoryTimeouts.SimpleTimeoutParser;
import org.jbehave.core.model.Scenario;
import org.jbehave.core.model.Story;
import org.jbehave.core.parsers.RegexStoryParser;
import org.jbehave.core.parsers.StepPatternParser;
import org.jbehave.core.parsers.gherkin.GherkinStoryParser;
import org.jbehave.core.steps.StepType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jdiesel.testframework.dslfilter.StepDefinitionMatcher.StepMatchResult;
import com.jdiesel.testframework.exception.TestFrameworkException;
import com.jdiesel.testframework.resourceloaders.AbsoluteLoadFromClasspath;

public class DslFilter {

	private final static Logger LOGGER = LoggerFactory.getLogger(DslFilter.class);
	private final static String generatedSourceFolder = "dsl-modified-features";
	public final static String ORIGINAL_RESOURCE_PATH = "src/test/resources/";
	final static String generatedResourcePath = ORIGINAL_RESOURCE_PATH + generatedSourceFolder;

	private enum BddFormat {
		GHERKIN, STORY;

		public static BddFormat of(Class<?> clazz) {
			if (clazz.equals(GherkinStoryParser.class)) {
				return GHERKIN;
			} else if (clazz.equals(RegexStoryParser.class)){
				return STORY;
			} else {
				throw new IllegalArgumentException("Do not know the BDD file format of type " + clazz.getName());
			}
		}
	}

	private static List<Class<?>> getStepDefinitionClasses(Class<?>[] instances, Class<?> embedder) {
		Objects.requireNonNull(instances, String.format(
				"%s did not have a UsingSteps annotation! Cannot determine template generation for scalable test framework.\nIf you cannot use the @UsingSteps annotations, override the getStepDefinitionClasses method and specify classes annotated with @DslStepDefinition.",
				embedder.getClass().getName()));
		List<Class<?>> classSpecificDslStepDefinitions = new ArrayList<>(instances.length);
		for (int i = 0; i < instances.length; i++) {
			// See if the steps being used are DslStepDefinitions
			DslStepDefinition stepDefinition = null;
			Class<?> clazz = instances[i];
			while (stepDefinition == null && clazz != Object.class) {
				if (clazz.getAnnotation(DslStepDefinition.class) != null) {
					classSpecificDslStepDefinitions.add(instances[i]);
				}
				clazz = clazz.getSuperclass();
			}
		}
		if (classSpecificDslStepDefinitions.size() == 0) {
			throw new TestFrameworkException(
					"None of the step candidates were marked with @DslStepDefinition. Cannot determine which steps are necessary for this test context.");
		}
		return classSpecificDslStepDefinitions;
	}

	public static List<String> createFilteredNaturalLanguageDslFiles(Embedder embedder, List<String> storyPaths,
			UsingSteps usingSteps, Class<?> testContext) {
		return createFilteredNaturalLanguageDslFiles(embedder, storyPaths, usingSteps.instances(), testContext);
	}

	private static String pathFromClassloader(String path) {
		ClassLoader loader = DslFilter.class.getClassLoader();
		path = path.replaceFirst("target/testclasses/", ORIGINAL_RESOURCE_PATH);
		return "file:" + loader.getResource(path).getPath();
	}

	public static List<String> createFilteredNaturalLanguageDslFiles(Embedder embedder, List<String> storyPaths,
			Class<?>[] usingSteps, Class<?> testContext) {
		// Separate generated files by test runner. Multiple test runners could
		// otherwise overwrite each other's files\
		// Unfortunately, the package names make the filepath so long that the file
		// cannot be made from a URL
		// So they need to be removed
		final String TEST_CONTEXT = testContext.getName()
				.split(testContext.getPackage()
				.getName())[1] // Get the classname without the package name
				.substring(1); // Remove the leading '.' before the classname
		StoryManager storyManager = new StoryManager(embedder.configuration()
				.useStoryLoader(new AbsoluteLoadFromClasspath()),
				embedder.stepsFactory(), null, null,
				null, new PerformableTree(), new SimpleTimeoutParser());
		List<Story> stories = null;
		try {
			stories = storyManager.storiesOfPaths(storyPaths);
		
		} catch (Exception e) {
			List<String> paths = storyPaths.stream().map(DslFilter::pathFromClassloader).collect(Collectors.toList());
			paths = paths.stream().map(s -> s.replaceFirst("target/test-classes/", ORIGINAL_RESOURCE_PATH)).collect(Collectors.toList());
			stories = storyManager.storiesOfPaths(paths);
		}
		
		// Find every step to check if it is superfluous to this local application
		// (e.g, if we are sharing a DSL test specification with the front end and back
		// end,
		// we don't need any steps related to browser navigation in the back end
		StepDefinitionMatcher matcher = new StepDefinitionMatcher();
		List<String> newStoryPaths = new ArrayList<>(stories.size());
		List<Class<?>> stepDefinitionClasses = getStepDefinitionClasses(usingSteps, testContext);
		LOGGER.info("Modifying natural language DSL to follow Interface Segregation Principle...");
		for (Story story : stories) {
			StepPatternParser parser = embedder.configuration().stepPatternParser();
			List<Scenario> refactoredScenarios = new ArrayList<>(story.getScenarios().size());
			for (Scenario scenario : story.getScenarios()) {
				List<String> refactoredSteps = refactorSteps(scenario.getSteps(), matcher, TEST_CONTEXT,
						stepDefinitionClasses, parser);
				refactoredScenarios.add(new Scenario(scenario.getTitle(), scenario.getMeta(),
						scenario.getGivenStories(), scenario.getExamplesTable(), refactoredSteps));
			}
			String newStoryPath = generatedResourcePath + "/" + TEST_CONTEXT + "/" + story.getPath().split(ORIGINAL_RESOURCE_PATH)[1];
			// Write the modified story to a new directory

			// Get lifecycle, or 'before/after' steps
			List<String> refactoredBefore = refactorSteps(story.getLifecycle().getBeforeSteps(Scope.SCENARIO), matcher,
					TEST_CONTEXT, stepDefinitionClasses, parser);
			List<String> refactoredAfter = refactorSteps(story.getLifecycle().getAfterSteps(Scope.SCENARIO), matcher,
					TEST_CONTEXT, stepDefinitionClasses, parser);
			Story refactoredStory = new Story(newStoryPath, story.getDescription(), story.getMeta(),
					story.getNarrative(), story.getGivenStories(), story.getLifecycle(), refactoredScenarios);
			try {
				LOGGER.info("Writing new natural language DSL file: " + newStoryPath);
				// Create the directory if it does not exist
				String relativeFilePath = null;
				if (newStoryPath.contains("target/test-classes/")) {
				 relativeFilePath = generatedResourcePath + "/" + TEST_CONTEXT + "/"
						+ newStoryPath.split("target/test-classes/")[1];
				} else {
					relativeFilePath = newStoryPath;
				}
				new File(Paths.get(relativeFilePath).getParent().toString()).mkdirs();
				FileOutputStream newGeneratedFile = new FileOutputStream(relativeFilePath, false);
				switch (BddFormat.of(embedder.configuration().storyParser().getClass())) {
				case GHERKIN:
					newGeneratedFile
							.write(DslFileWriter.storyObjectToGherkin(refactoredStory, refactoredBefore).getBytes());
					break;
				case STORY:
					newGeneratedFile.write(
							DslFileWriter.storyObjectToJbehaveStory(story, refactoredBefore, refactoredAfter).getBytes());
					/* not implemented: fall through */
				default:
					newGeneratedFile.close();
					throw new IllegalArgumentException(
							"Test framework did not know how to parse provided BDD file format. Do not know how to create files of type "
									+ embedder.configuration().storyParser().getClass().getName());
				}
				newGeneratedFile.close();
				if (embedder.configuration().storyParser().getClass().equals(GherkinStoryParser.class)) {

				} else {
					newGeneratedFile.close();

				}
			} catch (IOException e) {
				throw new TestFrameworkException(
						"Test framework could not write a refactored story to a new directory.\n\t" + e.getMessage());
			}

			newStoryPaths.add(
					generatedResourcePath + /* generatedSourceFolder */ "/" + TEST_CONTEXT + "/" + story.getPath()); // Leave
																														// off
																														// //
																														// directory
		}

		newStoryPaths = storyPaths.stream().map(p ->

		pathToGeneratedURL(p, TEST_CONTEXT)) // Convert to URL
				.collect(Collectors.toList());
		return newStoryPaths;
	}

	private static String pathToGeneratedURL(String path, String testContext) {
		return "file:" + FileSystems.getDefault().getPath("").toAbsolutePath() + "/" + generatedResourcePath + "/"
				+ testContext + "/" + path;
	}

	private static StepType getStepTypeFromString(String step) {
		return StepType.valueOf(step.substring(0, step.indexOf(' ')) //Get the first word
				.toUpperCase()); //Convert it to upper case and turn into an enum
	}
	
	private static List<String> refactorSteps(List<String> steps, StepDefinitionMatcher matcher, String testContext,
			List<Class<?>> stepDefinitionClasses, StepPatternParser parser) {
		if (steps.isEmpty()) { return new ArrayList<>(); }
		List<String> refactoredSteps = new ArrayList<>(steps.size());
		//Since we are commenting out superfluous steps, each step needs to explicitly
		//state it's own steptype (e.g, no 'And' can be used in steps.
		//This will allow us to avoid the following boundary case:
		/*
		 * Given a step
		 * #When step commented out by DSL filter
		 * And this step then becomes a 'Given' instead of the correct 'When'
		 */
		StepType stepType = getStepTypeFromString(steps.get(0));
		for (String step : steps) {
			StepType currentStepType = getStepTypeFromString(step);
			if (currentStepType.equals(StepType.AND)) {
				currentStepType = stepType;
			} else {
				stepType = currentStepType;
			}
			String stepWithoutPrefix = step.substring(step.indexOf(' ') + 1);
			LOGGER.debug("Searching for step pattern: " + stepWithoutPrefix);
			StepMatchResult result = matcher.matchStepInDefinitions(stepDefinitionClasses, step, parser);

			switch (result) {
			case DIRECT_MATCH:
			case NO_MATCH:
				// In the event the step doesn't match anything,
				// we will get a pending step error when running,
				// which is exactly what we want.
				// If it DOES match, then we want it exactly as it is
				refactoredSteps.add(stepType.name().substring(0, 1) 
						+ stepType.name().substring(1).toLowerCase() 
						+ " " + stepWithoutPrefix);
				break;
			case INDIRECT_MATCH:
				// If some other step definition matches,
				// we know that we are aware of the functionality in the DSL
				// but assume it is not relevant to this particular test context
				refactoredSteps.add(String.format("#%s #Step not contextually relevant to %s", step, testContext));
				break;
			default:
				throw new IllegalArgumentException("No logic to handle case " + result.name());
			}
		}
		return refactoredSteps;
	}

}
