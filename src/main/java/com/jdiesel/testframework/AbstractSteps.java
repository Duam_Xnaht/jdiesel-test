package com.jdiesel.testframework;

public class AbstractSteps {

	protected IllegalArgumentException enumException(Enum<?> e)  {
		throw new IllegalArgumentException("No implementation defined for " + e.name());
	}
}
