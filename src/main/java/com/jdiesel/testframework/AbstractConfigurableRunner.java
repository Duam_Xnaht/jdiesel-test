package com.jdiesel.testframework;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Properties;

import org.jbehave.core.InjectableEmbedder;
import org.jbehave.core.annotations.UsingSteps;
import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.failures.FailingUponPendingStep;
import org.jbehave.core.io.IOUtils;
import org.jbehave.core.io.InvalidStoryResource;
import org.jbehave.core.io.StoryFinder;
import org.jbehave.core.io.StoryLoader;
import org.jbehave.core.junit.JUnitStories;
import org.jbehave.core.reporters.CrossReference;
import org.jbehave.core.reporters.Format;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.junit.AfterClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jdiesel.testframework.documentation.DieselDocumentationGenerator;
import com.jdiesel.testframework.dslfilter.DslFileFinder;
import com.jdiesel.testframework.dslfilter.DslFilter;
import com.jdiesel.testframework.exception.TestFrameworkException;
import com.jdiesel.testframework.parser.JavaRegexParserWithNamedParameters;
import com.jdiesel.testframework.resourceloaders.AbsoluteLoadFromClasspath;


public abstract class AbstractConfigurableRunner extends JUnitStories {
	/**
	 * Annotation driven runners are typically preferred for their ease of
	 * configurability. However, since we need a spring component, we can't use
	 * the @UsingSteps annotation to configure the class as it would not create an
	 * instance from the spring context. Instead, we are extending JUnitStories so
	 * we can set up the configurableEmbedder and manually inject our steps into it.
	 */

	private final static Logger LOGGER = LoggerFactory.getLogger(AbstractConfigurableRunner.class);

	protected String metafilterExpression = "groovy: !SkipApi && !Skip && !Slow";

	/*
	 * @Autowired private InitApiStepImpl initSteps;
	 */
	abstract protected Optional<Object[]> getCandidateSteps();

	private final CrossReference xref = new CrossReference();

	protected static String documentationTitle = "Untitled";
	
	@AfterClass
	public static void doYourOneTimeTeardown() {
		LOGGER.info("Generating DSL Documentation...");
		new DieselDocumentationGenerator().generateDocumentation(documentationTitle);
	}

	@Override
	public InjectableStepsFactory stepsFactory() {
		UsingSteps usingSteps = this.getClass().getAnnotation(UsingSteps.class);
		if (getCandidateSteps().isPresent()) {
			return new InstanceStepsFactory(configuration(), getCandidateSteps().get());
		} else if (usingSteps != null && usingSteps.instances().length > 0) {
			List<Object> candidateSteps = new ArrayList<>(usingSteps.instances().length);
			for (int i = 0; i < usingSteps.instances().length; i++) {
				Object candidate;
				try {
					candidate = usingSteps.instances()[i].newInstance();
				} catch (InstantiationException | IllegalAccessException e) {
					throw new TestFrameworkException(
							"getCandidateSteps returned an empty result, so the @UsingSteps annotation was used to create candidate steps.\n"
									+ "However, while tryping to create an instance of \n\t"
									+ usingSteps.instances()[i].getClass() + "\n"
									+ "an exception occurred. You may need to provide a default constructor, or override getCandidateSteps to provide more complex objects."
									+ e.getMessage());

				}
				candidateSteps.add(candidate);
			}
			return new InstanceStepsFactory(configuration(), candidateSteps);
		} else {
			throw new TestFrameworkException(
					"No candidate steps provided. Either override getCandidateSteps or put a @UsingSteps annotation on the test runner"
							+ "\n\t" + this.getClass());
		}
	}

	public class ApiStoryLoader implements StoryLoader {

		@Override
		public String loadResourceAsText(String resourcePath) {
			InputStream stream = null;
			try {
				stream = new FileInputStream(resourcePath);
				return IOUtils.toString(stream, StandardCharsets.UTF_8, true);
			} catch (IOException e) {
				throw new InvalidStoryResource(resourcePath, stream, e);
			}
		}

		@Override
		public String loadStoryAsText(String storyPath) {
			return this.loadResourceAsText(storyPath);
		}
	}

	@Override
	public Configuration configuration() {
		Properties viewResources = new Properties();
		viewResources.put("decorateNonHtml", "true");
		return new MostUsefulConfiguration()
				.useStoryReporterBuilder(new StoryReporterBuilder())
				// .useStoryControls(new StoryControls().doResetStateBeforeScenario(true))
				.useStoryLoader(new AbsoluteLoadFromClasspath()).useFailureStrategy(new FailingUponPendingStep())
				.useStepPatternParser(new JavaRegexParserWithNamedParameters())
				.useStoryReporterBuilder(new StoryReporterBuilder().withViewResources(viewResources)
						.withFailureTrace(true).withFailureTraceCompression(true).withCrossReference(xref)
						.withFormats(Format.TXT, Format.ANSI_CONSOLE, Format.JSON));

	}

	@Test
	public void run() {

		LOGGER.info("Scalable Testing Framework configuring tests with Domain Specific Language");

		// We need to create new generated feature classes from those that were provided
		// Create a copy of each test file, but logically commenting out unnecessary
		// steps
		UsingSteps usingSteps = this.getClass().getAnnotation(UsingSteps.class);
		if (usingSteps.instances().length == 0) {
			throw new TestFrameworkException(
					"Runner not annotated with @UsingSteps. Please provide the classes containing the @DslStepDefinitions you are using so the DSL Filter can run your tests in the appropriate context.");
		}

		List<String> newStoryPaths = DslFilter.createFilteredNaturalLanguageDslFiles(configuredEmbedder(), storyPaths(),
				usingSteps.instances(), this.getClass());
		LOGGER.info("Test framework initialization complete!");
		LOGGER.info("Running tests...");
		configuredEmbedder().embedderControls().doFailOnStoryTimeout(false);
		configuredEmbedder().runStoriesAsPaths(newStoryPaths);
	}
	
	/*
	 * By default, get the stories specified by the @DslFileFinder annotation
	 */
	@Override
	public List<String> storyPaths() {
		// Get the feature files to run from the annotations
		// Be sure to include any on superclasses with the same annotation

		Class<?> clazz = this.getClass();
		List<String> includes = new ArrayList<>();
		List<String> excludes = new ArrayList<>();
		LOGGER.info("Finding natural language DSL test files");
		while (clazz != null && clazz != Object.class && clazz != InjectableEmbedder.class) {

			DslFileFinder[] dslStepDefinitions = clazz.getAnnotationsByType(DslFileFinder.class);
			for (int i = 0; i < dslStepDefinitions.length; i++) {
				for (int j = 0; j < dslStepDefinitions[i].includes().length; j++) {
					includes.add(dslStepDefinitions[i].includes()[j]);
				}
				for (int j = 0; j < dslStepDefinitions[i].excludes().length; j++) {
					excludes.add(dslStepDefinitions[j].excludes()[j]);
				}
			}
			clazz = clazz.getSuperclass();
		}
		return new StoryFinder().findPaths("src/test/resources", includes, excludes);
	}

}
