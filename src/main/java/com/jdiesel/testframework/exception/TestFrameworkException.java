package com.jdiesel.testframework.exception;

public class TestFrameworkException extends RuntimeException {

	private static final long serialVersionUID = 8853087401007443224L;

	public TestFrameworkException(String message)  {
		super(message);
	}
}
